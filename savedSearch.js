var splunkjs = require('splunk-sdk');

// Create a Service instance and log in 
var service = new splunkjs.Service({
  username:"readonly",
  password:"readonly1",
  scheme:"https",
  host:"csprspl04",
  port:"8089",
  version:"6.2.0"
});

// Print installed apps to the console to verify login
service.apps().fetch(function(err, apps) {
  if (err) {
    console.log("Error retrieving apps: ", err);
    return;
  }
});

// The saved search created earlier
var searchName = "wallOfShame60";

// Retrieve the saved search collection
var mySavedSearches = service.savedSearches();

mySavedSearches.fetch(function(err, mySavedSearches) {

  // Retrieve the saved search that was created earlier
  var mySavedSearch = mySavedSearches.item(searchName);

  // Run the saved search and poll for completion
  mySavedSearch.dispatch(function(err, job) {

    // Display the job's search ID
//    console.log("Job SID: ", job.sid);

    // Poll the status of the search job
    job.track({
      period: 200
    }, {
      done: function(job) {
 //       console.log("Done!");

        // Print out the statics
  //      console.log("Job statistics:");
   //     console.log("  Event count:  " + job.properties().eventCount);
  //      console.log("  Result count: " + job.properties().resultCount);
  //      console.log("  Disk usage:   " + job.properties().diskUsage + " bytes");
  //      console.log("  Priority:     " + job.properties().priority);

        // Get 10 results and print them
        job.results({
          count: 10
        }, function(err, results, job) {
          console.log(JSON.stringify(results));
        });
      },
      failed: function(job) {
        console.log("Job failed")
      },
      error: function(err) {
        done(err);
      }
    });
  });
});
